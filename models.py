from odoo import fields, models , api
from odoo.addons.payment.models.payment_acquirer import ValidationError
from dateutil.relativedelta import relativedelta
from datetime import datetime
class Admisio(models.Model):
    _inherit = 'op.admission.register'
    admision_id_js = fields.Many2one('op.admission.register',string="Admision MIgrar")
    def json_execute_create(self,table,data):
        table = str(table).replace('.', '_')
        filas_create = ''
        values_create= ''
        for d in data:
            filas_create = filas_create + ', ' + str(d)
            values_create = values_create + ', ' + str(data[d])
        filas_create = filas_create[1:]
        values_create = values_create[1:]
        create = "INSERT INTO  {tabla} ({filas}) VALUES ({values}) ;".format(tabla=table,
                                                                           filas=filas_create,
                                                                           values=values_create)
        #if table == "vex_logs":
        #self.env.cr.execute(create)
    def action_migrate(self):
        #buscar las lineas
        lineas_migrar = self.admision_id_js.admission_ids

        start_date = self.admision_id_js.start_date
        end_date = self.admision_id_js.end_date
        if not self.admision_id_js:
            raise ValidationError('ELija una Admision para Migrar')
        for l in self.admission_ids:
            for lm in lineas_migrar:
                if lm.student_id == l.student_id:
                    raise ValidationError('Ya se Genero una migracion para el alumnno '+str(l.name))
            if l.check == True:
                data = {
                     'name': "'{}'".format(l.name),
                     'register_id': self.admision_id_js.id,
                     'application_number' :  "'{}'".format(self.env['ir.sequence'].next_by_code('op.admission')),
                     'admission_date': "'{}'".format(l.admission_date),
                     'course_id': l.course_id.id,
                     'is_student': "'t'" if l.is_student else "'f'",
                     'birth_date': "'{}'".format(l.birth_date),
                     'email': "'{}'".format(l.email),
                     'first_name': "'{}'".format(l.first_name),
                     'last_name': "'{}'".format(l.last_name),
                     'application_date' : "'{}'".format(l.application_date)
                }
                if l.title:
                    data['title'] = l.title.id
                gender = l.gender
                if l.gender:
                    data['gender'] = "'{}'".format(gender)
                self.json_execute_create('op.admission',data)
                if l.discount:
                    data['discount'] = l.discount
                self.json_execute_create('op.admission',data)
                if l.due_date:
                    data['due_date'] = "'{}'".format(l.due_date)
                if l.fees:
                    data['fees'] =l.fees
                if l.fees_term_id:
                    data['fees_term_id'] = l.fees_term_id.id
                if l.student_id:
                    data['student_id'] = l.student_id.id
                if l.batch_id:
                    data['batch_id'] = l.batch_id.id
                if l.partner_id:
                    data['partner_id'] = l.partner_id.id

                #self.json_execute_create('op.admission',data)
                #record = self._cr.fetchall()
                #raise ValidationError(str(record))
                hoy = (datetime.today()).date()
                self.admision_id_js.start_date = l.admission_date
                self.admision_id_js.end_date = hoy

                record = self.env['op.admission'].create({
                    'name': l.name,
                    'register_id': self.admision_id_js.id,
                    'application_number': self.env['ir.sequence'].next_by_code('op.admission', sequence_date=None),
                    'title': l.title.id,
                    'admission_date': l.admission_date,
                    'application_date': l.application_date,
                    'course_id': l.course_id.id,
                    'discount': l.discount,
                    'due_date': l.due_date,
                    'is_student': l.is_student,
                    'student_id': l.student_id.id,
                    'batch_id': l.batch_id.id,
                    'fees': l.fees,
                    'fees_term_id': l.fees_term_id.id,
                    'partner_id': l.partner_id.id,
                    'birth_date': l.birth_date,
                    'email': l.email,
                    'first_name': l.first_name,
                    'gender': l.gender,
                    'last_name': l.last_name,

                })

                self.admision_id_js.start_date = start_date
                self.admision_id_js.end_date = end_date


                if record.fees_term_id:
                    val = []
                    product_id = record.register_id.product_id.id
                    for line in record.fees_term_id.line_ids:
                        no_days = line.due_days
                        per_amount = line.value
                        amount = (per_amount * record.fees) / 100
                        date = (datetime.today() + relativedelta(
                            days=no_days)).date()
                        dict_val = {
                            'fees_line_id': line.id,
                            'discount': record.discount or record.fees_term_id.discount,
                            'amount': amount,
                            'fees_factor': per_amount,
                            'date': date,
                            'product_id': product_id,
                            'state': 'draft',
                        }
                        val.append([0, False, dict_val])
                    record.student_id.write({
                        'fees_detail_ids': val
                    })


class AdmisioLine(models.Model):
    _inherit = 'op.admission'
    check = fields.Boolean()
